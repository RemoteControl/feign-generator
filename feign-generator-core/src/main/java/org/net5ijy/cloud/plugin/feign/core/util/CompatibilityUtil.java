package org.net5ijy.cloud.plugin.feign.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * CompatibilityUtil
 *
 * @author XGF
 * @date 2020/11/23 21:43
 */
@Deprecated
public class CompatibilityUtil {

  public static Object staticCodeLoadCompatibility() throws Exception {

    // 解决SpringComponentHolder里面的getComponent空指针问题
    Class<?> aClass;
    try {
      aClass = Class
          .forName("org.net5ijy.cloud.common.component.SpringComponentHolder");
    } catch (ClassNotFoundException e) {
      return null;
    }

    Class<?> name = Class.forName(
        "org.springframework.context.support.ClassPathXmlApplicationContext");

//    Class<?> aClass = Class
//        .forName("org.springframework.beans.factory.support.DefaultListableBeanFactory");

    Object o = name.newInstance();

    Method refresh = name.getMethod("refresh");
    refresh.invoke(o);

    Method start = name.getMethod("start");
    start.invoke(o);

//    Method getBeanFactory = name.getMethod("getBeanFactory");
//    Object invoke = getBeanFactory.invoke(o);

//    Method getBean = name.getMethod("getBean", Class.class);
//    Object invoke1 = getBean.invoke(o, MessageSource.class);

    Field applicationContext = aClass.getDeclaredField("applicationContext");
    applicationContext.setAccessible(true);

    applicationContext.set(null, o);

    return o;
  }
}
