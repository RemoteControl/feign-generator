package org.net5ijy.cloud.plugin.feign.core.util;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * SpringControllerUtil
 *
 * @author XGF
 * @date 2020/11/20 14:22
 */
public class SpringControllerUtil {

  public static String resolveUrlFromRequestMapping(RequestMapping requestMapping) {
    String[] value = requestMapping.value();
    if (value.length == 0) {
      return requestMapping.path()[0];
    }
    return value[0];
  }

  public static String resolveHttpMethodFromRequestMapping(RequestMapping requestMapping) {
    RequestMethod[] method = requestMapping.method();
    if (method.length == 0) {
      return "GET";
    }
    return method[0].toString();
  }

  public static String resolveUrlFromGetMapping(GetMapping getMapping) {
    String[] value = getMapping.value();
    if (value.length == 0) {
      return getMapping.path()[0];
    }
    return value[0];
  }

  public static String resolveUrlFromPostMapping(PostMapping postMapping) {
    String[] value = postMapping.value();
    if (value.length == 0) {
      return postMapping.path()[0];
    }
    return value[0];
  }

  public static String resolveUrlFromPutMapping(PutMapping putMapping) {
    String[] value = putMapping.value();
    if (value.length == 0) {
      return putMapping.path()[0];
    }
    return value[0];
  }

  public static String resolveUrlFromDeleteMapping(DeleteMapping deleteMapping) {
    String[] value = deleteMapping.value();
    if (value.length == 0) {
      return deleteMapping.path()[0];
    }
    return value[0];
  }
}
