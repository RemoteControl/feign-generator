package org.net5ijy.cloud.plugin.feign.core;

import org.net5ijy.cloud.plugin.feign.core.model.FeignClientClass;
import org.net5ijy.cloud.plugin.feign.core.model.FeignModel;
import org.net5ijy.cloud.plugin.feign.core.util.ZipUtil.Text;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * FeignClassAndModelResolver
 *
 * @author XGF
 * @date 2020/11/20 14:06
 */
public class FeignClassAndModelResolver {

  public static List<Text> resolveFeignClientClassList(
      List<FeignClientClass> feignClientClasses) throws IOException, TemplateException {


    return resolveFeignClientClassList(null,feignClientClasses);
  }

  public static List<Text> resolveFeignClientClassList(String basePackage,
          List<FeignClientClass> feignClientClasses) throws IOException, TemplateException {

    List<Text> pathAndFileContents = new ArrayList<>();

    for (FeignClientClass feignClientClass : feignClientClasses) {
      feignClientClass.setPackageName(basePackage);

      // 生成feign接口
      String feignClient = TemplateProcessor.parseFeignClient(feignClientClass);

      Text pathAndFileContent = new Text();

      pathAndFileContent.setText(feignClient);
      if(basePackage == null || "".equalsIgnoreCase(basePackage))
        pathAndFileContent.setFileName(feignClientClass.getFeignClassName() + ".java");
      else
        pathAndFileContent.setFileName(basePackage.replace('.', '/') + "/feign/"+feignClientClass.getFeignClassName() + ".java");


      pathAndFileContents.add(pathAndFileContent);

      // 生成fallback
      String feignClientFallBack = TemplateProcessor.parseFeignClientFallBack(feignClientClass);

      Text pathAndFileContent2 = new Text();

      pathAndFileContent2.setText(feignClientFallBack);
      if(basePackage == null || "".equalsIgnoreCase(basePackage))
        pathAndFileContent2
                .setFileName(
                        "fallback/" + feignClientClass.getFeignClassName() + "FallBackFactory.java");
      else
        pathAndFileContent2
                .setFileName(
                        basePackage.replace('.', '/') + "/feign/fallback/" + feignClientClass.getFeignClassName() + "FallBackFactory.java");

      pathAndFileContents.add(pathAndFileContent2);
    }

    return pathAndFileContents;
  }

  public static List<Text> resolveFeignModelList(List<FeignModel> feignModels)
      throws IOException, TemplateException {

    List<Text> pathAndFileContents = new ArrayList<>();

    for (FeignModel feignModel : feignModels) {

      String model = TemplateProcessor.parseFeignModel(feignModel);

      Text pathAndFileContent = new Text();

      pathAndFileContent.setText(model);

      pathAndFileContent.setFileName(
          feignModel.getPackageName().replace('.', '/') + "/" + feignModel.getModelName()
              + ".java");

      pathAndFileContents.add(pathAndFileContent);
    }

    return pathAndFileContents;
  }
}
