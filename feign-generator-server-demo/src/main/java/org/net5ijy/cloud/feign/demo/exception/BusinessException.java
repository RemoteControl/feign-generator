package org.net5ijy.cloud.feign.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常
 *
 * @author XGF
 * @date 2020/3/1 18:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class BusinessException extends RuntimeException {

  private String code;

  private String errorMessage;

  @Override
  public String getMessage() {
    return this.errorMessage;
  }

  @Override
  public String getLocalizedMessage() {
    return this.errorMessage;
  }
}
