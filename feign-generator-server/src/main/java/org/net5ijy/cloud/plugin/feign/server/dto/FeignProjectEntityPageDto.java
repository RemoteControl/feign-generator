package org.net5ijy.cloud.plugin.feign.server.dto;

import java.util.List;
import lombok.Data;
import org.net5ijy.cloud.plugin.feign.server.entity.FeignProjectEntity;

/**
 * FeignProjectEntityPageDto
 *
 * @author XGF
 * @date 2020/11/19 21:36
 */
@Data
public class FeignProjectEntityPageDto {

  private Integer total;

  private List<FeignProjectEntity> list;
}
