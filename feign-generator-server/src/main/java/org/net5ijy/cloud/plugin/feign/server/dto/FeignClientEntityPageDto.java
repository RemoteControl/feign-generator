package org.net5ijy.cloud.plugin.feign.server.dto;

import java.util.List;
import lombok.Data;
import org.net5ijy.cloud.plugin.feign.server.entity.FeignClientEntity;

/**
 * FeignClientEntityPageDto
 *
 * @author XGF
 * @date 2020/11/19 21:36
 */
@Data
public class FeignClientEntityPageDto {

  private Integer total;

  private List<FeignClientEntity> list;
}
